package utils

import (
	"fmt"
	"github.com/hashicorp/go-uuid"
	"log"
	"math/rand"
	"os"
	"time"
)

var random *rand.Rand

var Logger *log.Logger

func init() {
	logfile, err := os.Create("chat.log")
	if err != nil {
		fmt.Printf("error opening file: %v", err)
		os.Exit(1)
	}

	Logger = log.New(logfile, "", log.Lshortfile|log.LstdFlags)

	random = rand.New(rand.NewSource(time.Now().UnixNano()))
}

func NewId() string {
	id, _ := uuid.GenerateUUID()
	return id
}

func AddRandomDelay() {
	delay := time.Duration(random.Intn(10) * 100)
	time.Sleep(time.Millisecond * delay)
}

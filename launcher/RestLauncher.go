package launcher

import (
	"github.com/gofiber/fiber"
	"gitlab.com/indradeep/utter/controllers"
	"gitlab.com/indradeep/utter/repositories"
	"gitlab.com/indradeep/utter/services"
	"gitlab.com/indradeep/utter/utils"
)

type restLauncher struct {
	app *fiber.App
	port int
	speakerController *controllers.SpeakerController
}

func NewRestLauncher(port int) *restLauncher {
	ua := &restLauncher{app: fiber.New(), port:port}
	ua.createControllers()
	ua.createRoutes()
	return ua
}

//CreateServices creates the controllers
func (ua *restLauncher) createControllers() {

	// Speaker
	speakerRepository := repositories.NewMemorySpeakerRepository()
	speakerService := services.SpeakerService{ISpeakerRepository: speakerRepository}
	ua.speakerController = &controllers.SpeakerController{SpeakerService: speakerService}
}

func (ua *restLauncher) createRoutes() {
	ua.app.Post("/speakers", ua.speakerController.PostSpeaker())
	ua.app.Get("/speakers", ua.speakerController.GetSpeakers())
}

func (ua *restLauncher) Start() {
	err := ua.app.Listen(3000)

	if err != nil {
		utils.Logger.Println(err)
		panic(err)
	}
}

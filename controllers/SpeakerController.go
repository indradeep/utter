package controllers

import (
	"github.com/gofiber/fiber"
	"gitlab.com/indradeep/utter/models"
	"gitlab.com/indradeep/utter/services"
)

type SpeakerController struct {
	services.SpeakerService
}

type NewSpeakerRequest struct {
	Name string `json:"name" xml:"name" form:"name"`
	Email string `json:"email" xml:"email" form:"email"`
}

func (s SpeakerController) PostSpeaker () func (*fiber.Ctx) {
	return func (c *fiber.Ctx) {
		var speaker models.Speaker

		if err := c.BodyParser(&speaker); err != nil {
			c.Send("Error!")
		}

		if err := s.CreateSpeaker(&speaker); err != nil {
			c.Send("Error!")
		}

		_ = c.JSON(speaker)
	}
}


func (s SpeakerController) GetSpeakers () func (*fiber.Ctx) {
	return func (c *fiber.Ctx) {
		_ = c.JSON(s.ListSpeakers())
	}
}
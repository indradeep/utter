package repositories

import "gitlab.com/indradeep/utter/models"

type ISpeakerRepository interface {
	ListSpeakers() []*models.Speaker
	LoadSpeakerById(id string) (*models.Speaker, error)
	LoadSpeakerByEmail(id string) (*models.Speaker, error)
	SaveSpeaker(speaker *models.Speaker) error
}

package repositories

import (
	"fmt"
	"gitlab.com/indradeep/utter/models"
)

type MockSpeakerRepository struct {
	storeById map[string]*models.Speaker
	storeByEmail map[string]*models.Speaker
}

func (s MockSpeakerRepository) ListSpeakers() []*models.Speaker {
	speakers := make([]*models.Speaker, 0, len(s.storeById))

	for _, v:= range s.storeById {
		speakers = append(speakers, v)
	}

	return speakers
}

func NewMemorySpeakerRepository() MockSpeakerRepository {
	return MockSpeakerRepository{
		storeById:    make(map[string]*models.Speaker),
		storeByEmail: make(map[string]*models.Speaker),
	}
}

func (s MockSpeakerRepository) LoadSpeakerById(id string) (*models.Speaker, error) {
	speaker, ok := s.storeById[id]

	if !ok {
		return nil, fmt.Errorf("speaker by id %s not found", id)
	}

	return speaker, nil
}

func (s MockSpeakerRepository) LoadSpeakerByEmail(id string) (*models.Speaker, error) {
	speaker, ok := s.storeByEmail[id]

	if !ok {
		return nil, fmt.Errorf("speaker by id %s not found", id)
	}

	return speaker, nil
}

func (s MockSpeakerRepository) SaveSpeaker(speaker *models.Speaker) error {
	s.storeById[speaker.Id] = speaker
	s.storeByEmail[speaker.Email] = speaker

	return nil
}

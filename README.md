# Utter

Utter is a simplified Chat system written in Go as a learning exercise.
I am preparing for an interview, and thought this is the best way to
solidify my understanding of the language and its features!

## Features

### Application Features
* Groups conversations (Audiences)
* One-on one, Multi-person chat (Interactions)
* Membership (Speaker)

### Technical Features
* REST APIs
* WebSocket
* Pluggable Storage

## REST API

### Photos and Thumbnails

All IDs in the system are UUIDs, and hence unique.  You can query the 
image API with any ID with the expected size, and if the image exists
the API will return the `image/xxx` response with image data.

* Thumbnails: ```GET /images/{id}/thumbnail```
* Photos: ```GET /images/{id}/photo```


### Audience

#### Lists Audiences

Lists out all the audiences that I can be part of

`GET /audiences`

```json
{
  "audiences": [
    {
      "audienceId": "{audience id}",
      "audienceName": "{audience name}",
      "speakerCount": "{number of speakers}"
    }
  ] 
}
```

#### Audience Details

Provides the details of the audience and speakers that are part of it

`GET /audiences/{id}`

```json
{
  "audienceId": "{audience id}",
  "audienceName": "{audience name}",
  "speakers": [
    {
      "speakerId": "{speaker id}",
      "speakerName": "{speaker name}"
    } 
  ]
}
```

### Speakers

#### List Speakers

Lists all the speakers

`GET /speakers`

```json
{
  "speakers": [
    {
      "speakerId": "{speaker id}",
      "speakerName": "{speaker name}"
    } 
  ]
}
```

#### Speaker Details

Gets the profile of a specific speaker

`GET /speakers/{id}`

```json
{
  "speakerId": "{speaker id}",
  "speakerName": "{speaker name}",
  "email": "{speaker email}"
} 
```

### Utterances

#### Interaction

An interaction is a set of utterances between speakers / audience.

*Interaction Types:*

* `monologue`: Interaction with self
* `dialogue`: Interaction between two speakers
* `audience`: Interaction within the scope of an audience

*Speakers:*
* `monologue`/`audience` - one speaker
* `dialogue` - two speakers

`GET /interaction/{id}`

```json
{
  "interactionId": "{id}",
  "type": "{monologue/dialogue/audience}",
  "speakers": [
    {
      "speakerId": "{speaker id}",
      "speakerName": "{speaker name}"
    } 
  ],
  "last": "{iso8609 timestamp}"
}
```

#### Utterance

Utterances represent the exact content of the interaction - the messages
that are exchanged between speakers

`GET /interaction/{id}/utterances`

```json
{
  "interactionId": "{interaction id}",
  "utterances": [
    {
      "utteranceId": "{utterance id}",
      "speakerId": "{speaker id}",
      "type": "{text/attachment}",
      "body": "{utterance body}",
      "when": "{date time in ISO8609"
    } 
  ]
}
```

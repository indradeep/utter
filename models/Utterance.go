package models

import (
	"gitlab.com/indradeep/utter/utils"
	"time"
)

const (
	_ int = iota
	TEXT
	ATTACHMENT
)

type Utterance struct {
	Id string
	Speaker *Speaker
	Type int
	Body string
	When time.Time
}

func NewTextUtterance(speaker *Speaker, text string) *Utterance {
	return &Utterance{
		Id:      utils.NewId(),
		Speaker: speaker,
		Type:    TEXT,
		Body:    text,
		When:    time.Now(),
	}
}

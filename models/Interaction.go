package models

import "time"

const (
	_ int = iota
	MONOLOGUE
	DIALOGUE
	AUDIENCE
)

type Interaction struct {
	Id string
	Type int
	Speakers []*Speaker
	Utterances []*Utterance
	Last time.Time
}
module gitlab.com/indradeep/utter

go 1.14

require (
	github.com/gofiber/fiber v1.8.431
	github.com/hashicorp/go-uuid v1.0.2
)

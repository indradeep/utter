package main

import "gitlab.com/indradeep/utter/launcher"

func main() {
	restApi := launcher.NewRestLauncher(3000)
	restApi.Start()
}

package services

import (
	"gitlab.com/indradeep/utter/repositories"
	"testing"
)

func TestNewSpeakerService(t *testing.T) {
	repo := repositories.NewMemorySpeakerRepository()

	ss := NewSpeakerService(repo)

	name := "Tito"
	email := "tito@indradeep.com"

	speaker, err := ss.NewSpeaker(name, email)

	if err != nil || speaker == nil || speaker.Name != name || speaker.Email != email {
		t.Errorf("New Speaker couldn't be created")
	}
}

package services

import (
	"gitlab.com/indradeep/utter/models"
	"gitlab.com/indradeep/utter/repositories"
	"gitlab.com/indradeep/utter/utils"
)

type (

	//SpeakerService provides a set of operations on speaker
	SpeakerService struct {
		repositories.ISpeakerRepository
	}
)

//CreateSpeaker adds a new speaker to the repository
func (s SpeakerService) CreateSpeaker(speaker *models.Speaker) error {

	speaker.Id = utils.NewId()

	if err := s.SaveSpeaker(speaker); err != nil {
		return err
	}

	return nil
}